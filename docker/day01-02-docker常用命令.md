docker常用命令

## 一.常用命令

| 命令        | 作用                                       |
| --------- | ---------------------------------------- |
| attach    | 绑定到运行中容器的 标准输入, 输出,以及错误流（这样似乎也能进入容器内容，但是一定小心，他们操作的就是控制台，控制台的退出命令会生效，比如redis,nginx...） |
| build     | 从一个 Dockerfile 文件构建镜像                    |
| commit    | 把容器的改变 提交创建一个新的镜像                        |
| cp        | 容器和本地文件系统间 复制 文件/文件夹                     |
| create    | 创建新容器，但并不启动（注意与docker run 的区分）需要手动启动。start\stop |
| diff      | 检查容器里文件系统结构的更改【A：添加文件或目录 D：文件或者目录删除 C：文件或者目录更改】 |
| events    | 获取服务器的实时事件                               |
| exec      | 在运行时的容器内运行命令                             |
| export    | 导出容器的文件系统为一个tar文件。commit是直接提交成镜像，export是导出成文件方便传输 |
| history   | 显示镜像的历史                                  |
| images    | 列出所有镜像                                   |
| import    | 导入tar的内容创建一个镜像，再导入进来的镜像直接启动不了容器。/docker-entrypoint.sh nginx -g 'daemon ow;'docker ps --no-trunc 看下之前的完整启动命令再用他 |
| info      | 显示系统信息                                   |
| inspect   | 获取docker对象的底层信息                          |
| kill      | 杀死一个或者多个容器                               |
| load      | 从 tar 文件加载镜像                             |
| login     | 登录Docker registry                        |
| logout    | 退出Docker registry                        |
| logs      | 获取容器日志；容器以前在前台控制台能输出的所有内容，都可以看到          |
| pause     | 暂停一个或者多个容器                               |
| port      | 列出容器的端口映射                                |
| ps        | 列出所有容器                                   |
| pull      | 从registry下载一个image 或者repository          |
| push      | 给registry推送一个image或者repository           |
| rename    | 重命名一个容器                                  |
| restart   | 重启一个或者多个容器                               |
| rm        | 移除一个或者多个容器                               |
| rmi       | 移除一个或者多个镜像                               |
| run       | 创建并启动容器                                  |
| save      | 把一个或者多个镜像保存为tar文件                        |
| search    | 去docker hub寻找镜像                          |
| start     | 启动一个或者多个容器                               |
| stats     | 显示容器资源的实时使用状态                            |
| stop      | 停止一个或者多个容器                               |
| tag       | 给源镜像创建一个新的标签，变成新的镜像                      |
| top       | 显示正在运行容器的进程                              |
| unpause   | pause的反操作                                |
| update    | 更新一个或者多个docker容器配置                       |
| version   | Show the Docker version information      |
| container | 管理容器                                     |
| image     | 管理镜像                                     |
| network   | 管理网络                                     |
| volume    | 管理卷                                      |

## 二.常见部署案例 

### 1.部署Nginx 

```
# 注意 外部的/nginx/conf下面的内容必须存在，否则挂载会覆盖
docker run --name nginx-app \
-v /app/nginx/html:/usr/share/nginx/html:ro \
-v /app/nginx/conf:/etc/nginx
-d nginx
```



### 2.部署MySQL

```
# 5.7版本
docker run -p 3306:3306 --name mysql57-app \
-v /app/mysql/log:/var/log/mysql \
-v /app/mysql/data:/var/lib/mysql \
-v /app/mysql/conf:/etc/mysql/conf.d \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7



#8.x版本,引入了 secure-file-priv 机制，磁盘挂载将没有权限读写data数据，所以需要将权限透传，
或者chmod -R 777 /app/mysql/data
# --privileged 特权容器，容器内使用真正的root用户
docker run -p 3306:3306 --name mysql8-app \
-v /app/mysql/conf:/etc/mysql/conf.d \
-v /app/mysql/log:/var/log/mysql \
-v /app/mysql/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=123456 \
--privileged \
-d mysql


```



### 3.部署Redis 

```
# 提前准备好redis.conf文件，创建好相应的文件夹。如：
port 6379
appendonly yes
#更多配置参照 https://raw.githubusercontent.com/redis/redis/6.0/redis.conf
docker run -p 6379:6379 --name redis \
-v /app/redis/redis.conf:/etc/redis/redis.conf \
-v /app/redis/data:/data \
-d redis:6.2.1-alpine3.13 \
redis-server /etc/redis/redis.conf --appendonly yes
```



### 4.部署ElasticSearch 

```
#准备文件和文件夹，并chmod -R 777 xxx
#配置文件内容，参照
https://www.elastic.co/guide/en/elasticsearch/reference/7.5/node.name.html 搜索相
关配置
# 考虑为什么挂载使用esconfig ...
docker run --name=elasticsearch -p 9200:9200 -p 9300:9300 \
-e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms300m -Xmx300m" \
-v /app/es/data:/usr/share/elasticsearch/data \
-v /app/es/plugins:/usr/shrae/elasticsearch/plugins \
-v esconfig:/usr/share/elasticsearch/config \
-d elasticsearch:7.12.0
```



### 5.部署Tomcat 

```
# 考虑，如果我们每次 -v 都是指定磁盘路径，是不是很麻烦？
docker run --name tomcat-app -p 8080:8080 \
-v tomcatconf:/usr/local/tomcat/conf \
-v tomcatwebapp:/usr/local/tomcat/webapps \
-d tomcat:jdk8-openjdk-slim-buster
```



### 6.重启策略 

```
no，默认策略，在容器退出时不重启容器
on-failure，在容器非正常退出时（退出状态非0），才会重启容器
on-failure:3，在容器非正常退出时重启容器，最多重启3次
always，在容器退出时总是重启容器
unless-stopped，在容器退出时总是重启容器，但是不考虑在Docker守护进程启动时就已经停止了的容器
```



## docker exec 命令

```
docker exec -it alpine sh 
```



## docker build  命令

```
docker build -t imageName -f DockerfileName .
```



## docker push  命令

```
docker exec -it alpine sh 
```

## 三.网络和存储原理 

### 3.1)docker存储

####    1>.镜像如何存储 

1、Images and layers

​      Docker映像由一系列层组成。 每层代表图像的Dockerfile中的一条指令。 除最后一层外的每一层都是只
读的。 如以下Dockerfile： 

```
FROM ubuntu:15.04
COPY . /app
RUN make /app
CMD python /app/app.py

解释:
该Dockerfile包含四个命令，每个命令创建一个层。
	FROM语句从ubuntu：15.04映像创建一个图层开始。
	COPY命令从Docker客户端的当前目录添加一些文件。
	RUN命令使用make命令构建您的应用程序。
	最后，最后一层指定要在容器中运行的命令。
每一层只是与上一层不同的一组。 这些层彼此堆叠。
   创建新容器时，可以在基础层之上添加一个新的可写层。 该层通常称为“容器层”。 对运行中的容器所做的所有（例如写入新文件，修改现有文件和删除文件）都将写入此薄可写容器层。
```

2、Container and layers

​        容器和镜像之间的主要区别是可写顶层。

​        在容器中添加新数据或修改现有数据的所有写操作都存储在此可写层中。

​	删除容器后，可写层也会被删除。 基础图像保持不变。 因为每个容器都有其自己的可写容器层，并且所有更改都存储在该容器层中，所以多个容器可以共享对同一基础映像的访问，但具有自己的数据状态 



3、磁盘容量预估 

```
docker ps -s
size：用于每个容器的可写层的数据量（在磁盘上）。
virtual size：容器使用的用于只读图像数据的数据量加上容器的可写图层大小。
多个容器可以共享部分或全部只读图像数据。
从同一图像开始的两个容器共享100％的只读数据，而具有不同图像的两个容器（具有相同的层）共享这些公共
层。 因此，不能只对虚拟大小进行总计。这高估了总磁盘使用量，可能是一笔不小的数目。
```

4、镜像如何挑选 

```
busybox：是一个集成了一百多个最常用Linux命令和工具的软件。linux工具里的瑞士军刀
alpine：Alpine操作系统是一个面向安全的轻型Linux发行版经典最小镜像，基于busybox，功能比
Busybox完善。
slim：docker hub中有些镜像有slim标识，都是瘦身了的镜像。也要优先选择.无论是制作镜像还是下载镜像，优先选择alpine类型.
```



5、Copy On Write 

​	写时复制是一种共享和复制文件的策略，可最大程度地提高效率。
​	如果文件或目录位于映像的较低层中，而另一层（包括可写层）需要对其进行读取访问，则它仅使
用现有文件。
​	另一层第一次需要修改文件时（在构建映像或运行容器时），将文件复制到该层并进行修改。 这
样可以将I / O和每个后续层的大小最小化 

####    2>.容器如何挂载 

- Volumes(卷) ：存储在主机文件系统的一部分中，该文件系统由Docker管理（在Linux上是“ / var /lib / docker / volumes /”）。 非Docker进程不应修改文件系统的这一部分。 卷是在Docker中持久存储数据的最佳方法。

  ​

- Bind mounts(绑定挂载) 可以在任何地方 存储在主机系统上。 它们甚至可能是重要的系统文件或目录。 Docker主机或Docker容器上的非Docker进程可以随时对其进行修改。

  ​

- tmpfs mounts(临时挂载) 仅存储在主机系统的内存中，并且永远不会写入主机系统的文件系统 



1、volume(卷) 

- 匿名卷使用

  ```
  docker run -dP -v :/etc/nginx nginx
  #docker将创建出匿名卷，并保存容器/etc/nginx下面的内容
  ```

  ​

- 具名卷使用

  ```
  docker run -dP -v nginx:/etc/nginx nginx
  #docker将创建出名为nginx的卷，并保存容器/etc/nginx下面的内容
  ```

   如果将空卷装入存在文件或目录的容器中的目录中，则容器中的内容（复制）到该卷中。
  如果启动一个容器并指定一个尚不存在的卷，则会创建一个空卷 



2、bind mount

​       如果将绑定安装或非空卷安装到存在某些文件或目录的容器中的目录中，则这些文件或目录会被安装遮盖，就像您将文件保存到Linux主机上的/mnt中一样，然后将USB驱动器安装到/mnt中。在卸载USB驱动器之前，/mnt的内容将被USB驱动器的内容遮盖。 被遮盖的文件不会被删除或更改，但是在安装绑定安装或卷时将无法访问。



总结：外部目录覆盖内部容器目录内容，但不是修改。所以谨慎，外部空文件夹挂载方式也会导致容器内部是空文件夹  



```
docker run -dP -v /my/nginx:/etc/nginx nginx
# bind mount和 volumes 的方式写法区别在于
# 所有以/开始的都认为是 bind mount ，不以/开始的都认为是 volumes.
```

3、管理卷 

```
docker volume create xxx：创建卷名
docker volume inspect xxx：查询卷详情
docker volume ls: 列出所有卷
docker volume prune: 移除无用卷
```



4、docker cp 

cp的细节 

```
docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|- ：把容器里面的复制出来
docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH：把外部的复制进去
```

- SRC_PATH 指定为一个文件 

  -  DEST_PATH 不存在：文件名为 DEST_PATH ，内容为SRC的内容
  -  DEST_PATH 不存在并且以 / 结尾：报错
  -  DEST_PATH 存在并且是文件：目标文件内容被替换为SRC_PATH的文件内容。
  -  DEST_PATH 存在并且是目录：文件复制到目录内，文件名为SRC_PATH指定的名字 

- SRC_PATH 指定为一个目录 

  - DEST_PATH 不存在： DEST_PATH 创建文件夹，复制源文件夹内的所有内容

  - DEST_PATH 存在是文件：报错

  - DEST_PATH 存在是目录

    - SRC_PATH 不以 /. 结束：源文件夹复制到目标里面


    - SRC_PATH 以 /. 结束：源文件夹里面的内容复制到目标里面

  自动创建文件夹不会做递归。把父文件夹做好

  [root@lfy ~]# docker cp index.html mynginx4:/usr/share/nginx/html

  [root@lfy ~]# docker cp mynginx4:/etc/nginx/nginx.conf nginx.conf 

### 3.2)Docker网络

#### 1.端口映射

```
docker create -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 --name hello-mysql
mysql:5.7
```

#### 2.容器互联

--link name:alias ，name连接容器的名称，alias连接的别名
场景：我们无需暴露mysql的情况下，让web应用使用mysql； 

```
docker run -d -e MYSQL_ROOT_PASSWORD=123456 --name mysql01 mysql:5.7
docker run -d --link mysql01:mysql --name tomcat tomcat:7
docker exec -it tomcat bash
cat /etc/hosts
ping mysql
```

#### 3.自定义网络 

- 默认网络原理 

  Docker使用Linux桥接，在宿主机虚拟一个Docker容器网桥(docker0)，Docker启动一个容器时会根据Docker网桥的网段分配给容器一个IP地址，称为Container-IP，同时Docker网桥是每个容器的默认网关。因为在同一宿主机内的容器都接入同一个网桥，这样容器之间就能够通过容器的Container-IP直接通信 

  ​

  Docker容器网络就很好的利用了Linux虚拟网络技术，在本地主机和容器内分别创建一个虚拟接口，并让他们彼此联通（这样一对接口叫veth pair）；

  ​

  Docker中的网络接口默认都是虚拟的接口。虚拟接口的优势就是转发效率极高（因为Linux是在内核中进行数据的复制来实现虚拟接口之间的数据转发，无需通过外部的网络设备交换），对于本地系统和容器系统来说，虚拟接口跟一个正常的以太网卡相比并没有区别，只是他的速度快很多。 

     

  ```
  原理：
  1、每一个安装了Docker的linux主机都有一个docker0的虚拟网卡。桥接网卡
  2、每启动一个容器linux主机多了一个虚拟网卡。
  3、docker run -d -P --name tomcat --net bridge tomcat:8
  ```

  ​

- 网络模式 

   

  | 网络模式        | 配置                      | 说明                                       |
  | ----------- | ----------------------- | ---------------------------------------- |
  | bridge模式    | --net=bridge            | 默认值，在Docker网桥docker0上为容器创建新的网络栈          |
  | none模式      | --net=none              | 不配置网络，用户可以稍后进入容器，自行配置                    |
  | container模式 | --net=container:name/id | 容器和另外一个容器共享Network namespace。kubernetes中的pod就是多个容器共享一个Networknamespace。 |
  | host模式      | --net=host              | 容器和宿主机共享Network namespace                |
  | 用户自定义       | 配置 --net=自定义网络          | 说明 创建容器的时候可以指定为自己定义的网络                   |

  ​

- 自建网络测试 

  ```yml
  #1、docker0网络的特点。，
  默认、域名访问不通、--link 域名通了，但是删了又不行


  #2、可以让容器创建的时候使用自定义网络
  1、自定义创建的默认default "bridge"
  2、自定义创建一个网络网络
  docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet

  所有东西实时维护好，直接域名ping通
  docker network connect [OPTIONS] NETWORK CONTAINER



  #3、跨网络连接别人就用。把tomcat加入到mynet网络
  docker network connect mynet tomcat

  效果：
  1、自定义网络，默认都可以用主机名访问通
  2、跨网络连接别人就用 docker network connect mynet tomcat


  #4、命令
  1、容器启动，指定容器ip。 docker run --ip 192.168.0.3 --net 自定义网络
  2、创建子网。docker network create --subnet 指定子网范围 --driver bridge 所有东西实时
  维护好，直接域名ping同
  3、docker compose 中的网络默认就是自定义网络方式。
  ```

  ​













