### 一.FROM

​	FROM 指定基础镜像，最好挑一些apline，slim之类的基础小镜像. 

### 二.LABLE

​	标注镜像的一些说明信息 

### 三.RUN

- RUN指令在当前镜像层顶部的新层执行任何命令，并提交结果，生成新的镜像层 
- 生成的提交映像将用于Dockerfile中的下一步。 分层运行RUN指令并生成提交符合Docker的核心概念，就像源代码控制一样。


- exec形式可以避免破坏shell字符串，并使用不包含指定shell可执行文件的基本映像运行RUN命令。可以使用SHELL命令更改shell形式的默认shell。 在shell形式中，您可以使用\（反斜杠）将一条RUN指令继续到下一行。 

  总结：什么是shell和exec形式 

  ```
  1. shell 是 /bin/sh -c <command>的方式，
  2. exec ["/bin/sh","-c",command] 的方式== shell方式
  也就是exec 默认方式不会进行变量替换
  ```

  ​

### 四.CMD和ENTRYPOINT 

​    1>.都可以作为容器启动入口 

​        	 CMD 的三种写法：

- CMD ["executable","param1","param2"] ( exec 方式, 首选方式)

- CMD ["param1","param2"] (为ENTRYPOINT提供默认参数)

- CMD command param1 param2 ( shell 形式)

  ​

  ​	ENTRYPOINT 的两种写法：

- ENTRYPOINT ["executable", "param1", "param2"] ( exec 方式, 首选方式)

- ENTRYPOINT command param1 param2 (shell 形式)

```
# 一个示例
FROM alpine
LABEL maintainer=leifengyang
CMD ["1111"]
CMD ["2222"]
ENTRYPOINT ["echo"]
#构建出如上镜像后测试
docker run xxxx：效果 echo 1111
```

1、只能有一个CMD

- Dockerfile中只能有一条CMD指令。 如果您列出多个CMD，则只有最后一个CMD才会生效。
- CMD的主要目的是为执行中的容器提供默认值。 这些默认值可以包含可执行文件，也可以省略可执行文件，在这种情况下，您还必须指定ENTRYPOINT指令。

2 、CMD为ENTRYPOINT提供默认参数 

- 如果使用CMD为ENTRYPOINT指令提供默认参数，则CMD和ENTRYPOINT指令均应使用JSON数组格

  式指定。 

3、组合最终效果 

|                          | 无 ENTRYPOINT       | ENTRYPOINTexec_entry p1_entry | ENTRYPOINT[“exec_entry”, “p1_entry”] |
| ------------------------ | ------------------ | ----------------------------- | ------------------------------------ |
| 无CMD                     | 错误, 不允许的写法         | /bin/sh -c exec_entryp1_entry | exec_entry p1_entry                  |
| CMD[“exec_cmd”,“p1_cmd”] | exec_cmdp1_cmd     | /bin/sh -c exec_entryp1_entry | exec_entry p1_entryexec_cmd p1_cmd   |
| CMD[“p1_cmd”,“p2_cmd”]   | p1_cmdp2_cmd       | /bin/sh -c exec_entryp1_entry | exec_entry p1_entryp1_cmd p2_cmd     |
| CMD exec_cmd             | /bin/sh -cexec_cmd | /bin/sh -c exec_entry         | exec_entry p1_entry /bin/sh          |

4、docker run启动参数会覆盖CMD内容 

```
# 一个示例
FROM alpine
LABEL maintainer=leifengyang
CMD ["1111"]
ENTRYPOINT ["echo"]
#构建出如上镜像后测试
docker run xxxx：什么都不传则 echo 1111
docker run xxx arg1：传入arg1 则echo arg1
```

### 五.ARG和ENV

1、ARG

- ARG指令定义了一个变量，用户可以在构建时使用--build-arg = 传递，docker build命令会将其传递给构建器。
- --build-arg 指定参数会覆盖Dockerfile 中指定的同名参数
- 如果用户指定了 未在Dockerfile中定义的构建参数 ，则构建会输出 警告 。
- ARG只在构建期有效，运行期无效

2、ENV

- 在构建阶段中所有后续指令的环境中使用，并且在许多情况下也可以内联替换。
- 引号和反斜杠可用于在值中包含空格。
- ENV 可以使用key value的写法，但是这种不建议使用了，后续版本可能会删除 

### 六.ADD和COPY

1、COPY
COPY的两种写法

```
COPY [--chown=<user>:<group>] <src>... <dest>
COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]
```

--chown功能仅在用于构建Linux容器的Dockerfiles上受支持，而在Windows容器上不起作用

COPY指令从 src 复制新文件或目录，并将它们添加到容器的文件系统中，路径为 dest 。

可以指定多个 src 资源，但是文件和目录的路径将被解释为相对于构建上下文的源。

每个 src 都可以包含通配符，并且匹配将使用Go的filepath.Match规则进行

```yaml
COPY hom* /mydir/ 				#当前上下文，以home开始的所有资源
COPY hom?.txt /mydir/ 			 # ?匹配单个字符
COPY test.txt relativeDir/ 		  # 目标路径如果设置为相对路径，则相对与 WORKDIR 开始
# 把 “test.txt” 添加到 <WORKDIR>/relativeDir/

COPY test.txt /absoluteDir/ 		#也可以使用绝对路径，复制到容器指定位置


#所有复制的新文件都是uid(0)/gid(0)的用户，可以使用--chown改变
COPY --chown=55:mygroup files* /somedir/
COPY --chown=bin files* /somedir/
COPY --chown=1 files* /somedir/
COPY --chown=10:11 files* /somedir/
```

2、ADD
同COPY用法，不过 ADD拥有自动下载远程文件和解压的功能。
注意： 

- src 路径必须在构建的上下文中； 不能使用 ../something /something 这种方式，因为docker构建的第一步是将上下文目录（和子目录）发送到docker守护程序。

- 如果 src 是URL，并且 dest 不以斜杠结尾，则从URL下载文件并将其复制到 dest 。

   	如果 dest 以斜杠结尾，将自动推断出url的名字（保留最后一部分），保存到 dest

- 如果 src 是目录，则将复制目录的整个内容，包括文件系统元数据。 

### 七.WORKDIR和VOLUMN

​	1、WORKDIR

- WORKDIR指令为Dockerfile中跟随它的所有 RUN，CMD，ENTRYPOINT，COPY，ADD 指令设置工作目

  录。 如果WORKDIR不存在，即使以后的Dockerfile指令中未使用它也将被创建。

- WORKDIR指令可在Dockerfile中多次使用。 如果提供了相对路径，则它将相对于上一个WORKDIR指

  令的路径。 例如：

```
WORKDIR /a
WORKDIR b
WORKDIR c
RUN pwd
#结果 /a/b/c
```

-  也可以用到环境变量 

  ```yaml
  ENV DIRPATH=/path
  WORKDIR $DIRPATH/$DIRNAME
  RUN pwd
  #结果 /path/$DIRNAME
  ```

2、VOLUME
作用：把容器的某些文件夹映射到主机外部
写法： 

```
VOLUME ["/var/log/"] #可以是JSON数组
VOLUME /var/log #可以直接写
VOLUME /var/log /var/db #可以空格分割多个
```

***注意：
用 VOLUME 声明了卷，那么以后对于卷内容的修改会被丢弃，所以， 一定在volume声明之前修改内容 

### 八.USER

USER指令设置运行映像时要使用的用户名（或UID）以及可选的用户组（或GID），以及Dockerfile
中USER后面所有RUN，CMD和ENTRYPOINT指令。 

### 九.EXPOSE

- EXPOSE指令通知Docker容器在运行时在指定的网络端口上进行侦听。 可以指定端口是侦听TCP还是UDP，如果未指定协议，则默认值为TCP。
- EXPOSE指令实际上不会发布端口。 它充当构建映像的人员和运行容器的人员之间的一种文档，即有关打算发布哪些端口的信息。 要在运行容器时实际发布端口，请在docker run上使用-p标志发布并映射一个或多个端口，或使用-P标志发布所有公开的端口并将其映射到高阶端口 

### 重点:multi-stage builds

解决：如何让一个镜像变得更小; 多阶段构建的典型示例 

#### 1、使用 

```yaml
### 我们如何打包一个Java镜像
FROM maven
WORKDIR /app
COPY . .
RUN mvn clean package
COPY /app/target/*.jar /app/app.jar
ENTRYPOINT java -jar app.jar
## 这样的镜像有多大？
## 我们最小做到多大？
```

#### 2、生产示例 

```yaml
#以下所有前提 保证Dockerfile和项目在同一个文件夹
# 第一阶段：环境构建;
FROM maven:3.5.0-jdk-8-alpine AS builder
WORKDIR /app
ADD ./ /app
RUN mvn clean package -Dmaven.test.skip=true
# 第二阶段，最小运行时环境，只需要jre
FROM openjdk:8-jre-alpine
# 修改时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai'>/etc/timezone
LABEL maintainer="534096094@qq.com"
# 从上一个阶段复制内容
COPY --from=builder /app/target/*.jar /app.jar
ENV JAVA_OPTS=""
ENV PARAMS=""
# 运行jar包
ENTRYPOINT [ "sh", "-c", "java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS
-jar /app.jar $PARAMS" ]

```







```xml
<!--为了加速下载需要在pom文件中复制如下 -->
<repositories>
      <repository>
          <id>aliyun</id>
          <name>Nexus Snapshot Repository</name>
          <url>https://maven.aliyun.com/repository/public</url>
          <layout>default</layout>
          <releases>
              <enabled>true</enabled>
          </releases>
          <!--snapshots默认是关闭的,需要开启 -->
          <snapshots>
              <enabled>true</enabled>
          </snapshots>
      </repository>
</repositories>
<pluginRepositories>
      <pluginRepository>
          <id>aliyun</id>
          <name>Nexus Snapshot Repository</name>
          <url>https://maven.aliyun.com/repository/public</url>
          <layout>default</layout>
          <releases>
              <enabled>true</enabled>
          </releases>
          <snapshots>
              <enabled>true</enabled>
          </snapshots>
      </pluginRepository>
</pluginRepositories>
```



#### Images瘦身实践  

需要注意的点:

- ### 选择最小的基础镜像

- 合并RUN环节的所有指令，少生成一些层

- RUN期间可能安装其他程序会生成临时缓存，要自行删除。如： 

- ```yaml
  RUN apt-get update && apt-get install -y \
  bzr \
  cvs \
  git \
  mercurial \
  subversion \
  && rm -rf /var/lib/apt/lists/*
  ```

- 使用 .dockerignore 文件，排除上下文中无需参与构建的资源
  使用多阶段构建
  合理使用构建缓存加速构建。--no-cache 