```yaml
version: '3'
services:
  elasticsearch:
    image: elasticsearch:6.4.0
    container_name: elasticsearch
    environment:
      - "cluster.name=elasticsearch" #设置集群名称为elasticsearch
      - "discovery.type=single-node" #以单一节点模式启动
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" #设置使用jvm内存大小
    volumes:
      - /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载
      - /mydata/elasticsearch/data:/usr/share/elasticsearch/data #数据文件挂载
    ports:
      - 9200:9200
  kibana:
    image: kibana:6.4.0
    container_name: kibana
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    environment:
      - "elasticsearch.hosts=http://es:9200" #设置访问elasticsearch的地址
    ports:
      - 5601:5601
  logstash:
    image: logstash:6.4.0
    container_name: logstash
    volumes:
      - /mydata/logstash/logstash-springboot.conf:/usr/share/logstash/pipeline/logstash.conf #挂载logstash的配置文件
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    ports:
      - 4560:4560
```



以上为linux配置ELK环境的docker-compones.yaml文件

**注意**:docker在windowns下 volumes 配置为：

  


| - D:/docker-data/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载 |
| ---------------------------------------- |
| - D:/docker-data/logstash/logstash-springboot.conf:/usr/share/logstash/pipeline/logstash.conf #挂载logstash的配置文件 |

```yaml
version: '3'
services:
  elasticsearch:
    image: elasticsearch:6.4.0
    container_name: elasticsearch6
    environment:
      - "cluster.name=elasticsearch" #设置集群名称为elasticsearch
      - "discovery.type=single-node" #以单一节点模式启动
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" #设置使用jvm内存大小
    volumes:
      - D:/docker-data/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载
      - D:/docker-data/elasticsearch/data:/usr/share/elasticsearch/data #数据文件挂载
    ports:
      - 9200:9200
  kibana:
    image: kibana:6.4.0
    container_name: kibana6
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    environment:
      - "elasticsearch.hosts=http://es:9200" #设置访问elasticsearch的地址
    ports:
      - 5601:5601
  logstash:
    image: logstash:6.4.0
    container_name: logstash6
    volumes:
      - D:/docker-data/logstash/logstash-springboot.conf:/usr/share/logstash/pipeline/logstash.conf #挂载logstash的配置文件
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    ports:
      - 4560:4560
```

 

以上为windows配置ELK环境的docker-compones.yaml文件

---

---



# Docker 容器常用命令

### Docker 环境安装

```
1.安装yum-utils：
yum install -y yum-utils device-mapper-persistent-data lvm2
2.为yum源添加docker仓库位置：
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
3.安装docker:
yum install docker-ce
4.启动docker:
systemctl start docker
```

### 列出镜像     

```
docker images
```

### 删除镜像     

* 指定名称删除镜像 

```
docker  rmi  java:8 
```

* 指定名称删除镜像（强制）

```
docker rmi -f java:8
```

* 强制删除所有镜像

```
docker rmi -f $(docker images)
```

### 新建并启动容器

```
docker run -p 9200:9200 --name elasticsearch  -d elasticsearch:6.4.0
```

* -d选项：表示后台运行
* --name选项：指定运行后容器的名字为nginx,之后可以通过名字来操作容器
* -p选项：指定端口映射，格式为：hostPort:containerPort

### 列出所有镜像

* 列出运行中的容器：

```
docker ps
```

* 列出所有容器

```
docker ps -a
```

### 停止容器

```
docker stop  $ContainerName(或者$ContainerId)
```

比如：

```
docker stop nginx
或者
docker stop c5f5d5125587
```

### 强制停止容器

```
docker kill  $ContainerName(或者$ContainerId)
```

### 启动已停止的容器

```
docker start  $ContainerName(或者$ContainerId)
```

### 进入容器

* 先查询出容器的pid：

```
docker inspect --format "{{.State.Pid}}" $ContainerName(或者$ContainerId)
```

* 根据容器的pid进入容器：

```reStructuredText
nsenter --target "$pid" --mount --uts --ipc --net --pid
```

### 删除容器

* 删除指定容器：

```
docker rm $ContainerName(或者$ContainerId)
```

* 强制删除所有容器；

```
docker rm -f $ContainerName(或者$ContainerId)
```

### 查看容器的日志

```
docker log $ContainerName(或者$ContainerId)
```

### 查看容器的IP地址

```
docker inspect --format '{{ .NetworkSettings.IPAddress }}' $ContainerName(或者$ContainerId)
```

### 同步宿主机时间到容器

```
docker cp /etc/localtime $ContainerName(或者$ContainerId)
```

### 在宿主机查看docker使用cpu、内存、网络、io情况

* 查看指定容器情况：

```
docker stats $ContainerName(或者$ContainerId)
```

* 查看所有容器情况：

```
docker stats -a 
```

### 进入Docker容器内部的bash

```
docker exec -it $ContainerName  /bin/bash
```

### 修改Docker镜像的存放位置

* 查看Docker镜像的存放位置：

```
docker info | grep "Docker Root Dir"
```

* 关闭Docker服务：

```
systemctl stop docker 
```

* 移动目录到目标路径：

```
mv /var/lib/docker/ mydata/docker
```

* 建立软连接：

```
ln -s /mydata/docker /var/lib/docker/ 
```



---

---



dodocker run --rm -v d:/test:/data hello-worldcker run --name centos7 -it -v D:\docker-data\exchange\:/mnt centos:7



```
[root@Ieat4 ~]# sudo docker run --name rancher-server -d --restart=unless-stopped -p 18180:8080 rancher/server

```

docker run  -p 8000:8080 --name rancher-server -d  rancher/server -v D:/docker-data/rancher/localtimero:/etc/localtime:ro  

docker run -p 9200:9200 --name elasticsearch  -d elasticsearch:6.4.0

D:/docker-data/elasticsearch/plugins:/usr/share/elasticsearch/plugins 