## 1、Pod 概念

 ```\
Pod基本概念
 1、最小部署的单元
 2、包括多个容器
 3、一个pod中容器共享网络命名空间
 4、pod是短暂的
 ```



## 2、Pod存在的意义

```
1、创建容器使用docker，一个docker对应一个容器、一个容器有一个进程，一个容器运行一个应用程序
2、Pod的是多进程设计，运行多个应用程序
  一个pod有多个容器，一个容器里面运行一个应用程勋
3、pod存在为了亲密型应用
  两个应用之间进行交互
  网络之间调用
  两个应用需要频繁调用
```



## 3、pod的实现机制

```
    pod实现共享网络机制：
     首先通过Pause容器，把其他业务的容器加到Pause容器里面，让所有的业务容器在同一个命名空间（有相同的IP mac port）
    
    共享存储：
    引入数据卷概念volum，使用数据卷进行持久化存储
      
      
```



## 4、Pod镜像拉取的策略

```
 IfNotPresent 默认值，镜像在宿主机上不存在时才拉取
 Always  每次创建POd都会重新拉取一次镜像
 Never Pod永远不会主动拉取镜像
 
 例子：
    spec:
      containers:
        - name: nginx
          image: ninx:1.4
          imagePullPolicy: Always
```



## 5、Pod的资源限制

```
例子：
resources：
  requests：<调度>
    memory: "64Mi"
    cpu: "250m"
  limits:  <最大的资源限制>
    memory: "128Mi"
    cpu: "500m" 
```



## 6、pod的重启机制

```
 Always 当容器终止退出后，总是重启容器，默认策略
 OnFailure  当容器异常退出（退出状态码非0）时，才重启容器
 Never 当容器终止退出，从不重启容器、
 
例子：
 	restartPolicy: Never
```



## 7、Pod的健康检查

```
#livenessProbe 存活检查
如果检查失败，将杀死容器，根据Pod的restartPolicy来操作

readinessProbe 就绪检查
如果检查失败，Kubernetes会把pod从service endpoints中剔除

Probe支持一下三种检查方法：

httpGet
  发送http请求，返回200-400范围状态码为成功

exec
  执行shell命令返回状态码是0位成功
 
 tcpSocket
  发送TCP socket建立成功
```



8、创建Pod的流程

```

```





9、















