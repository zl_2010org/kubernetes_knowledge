k8s的部署

| 角色     | IP              |
| ------ | --------------- |
| master | 192.168.137.150 |
| node1  | 192.168.137.152 |
| node2  | 192.168.137.153 |

# Kubeadm安装步骤如下

## 1、关闭防火墙

```
$ systemctl stop firewalld

$ systemctl disable firewalld
```



## 2、关闭selinux：

 ```
 sed -i "s/^SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

 sed -i "s/^SELINUX=permissive/SELINUX=disabled/g" /etc/selinux/config

 setenforce 0

 ```



## 3、关闭swap：

```
 临时方式 ：
 swapoff -a     
 永久方式 ：
 vim /etc/fstab  
  $ sed -i 's/.swap./#&/' /etc/fstab
  cat /etc/fstab

```



## 4、将桥接的IPv4流量传递到iptables的链：

```
$ cat > /etc/sysctl.d/k8s.conf << EOF
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables = 1
  EOF
生效：
$ sysctl --system

```



## 5、同步时间

```
yum install ntpdate -y

ntpdate time.windows.com
```



## 6、安装docker（所有节点）

```
下载docker：
     wget https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo -O 

/etc/yum.repos.d/docker-ce.repo

$ yum -y install docker-ce-18.06.1.ce-3.el7

设置docker开启自启
$ systemctl enable docker && systemctl start docker

查看docker的版本
$ docker --version
	Docker version 18.06.1-ce, build e68fc7a

```



## 7、 设置仓库地址

	登录阿里云
	cat /etc/docker/daemon.json <<-'EOF'
	{
	  "registry-mirrors": ["https://jah9pd9n.mirror.aliyuncs.com"]
	}
	EOF
## 8、添加阿里云YUM软件源

```
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]

name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg

EOF
```

## 

## 9、刷新yum源缓存

```
yum clean all
yum makecache
yum repolist

```

## 10、安装kubeadm，kubelet和kubectl

```
三个节点都需要安装
yum install -y kubelet-1.18.0 kubeadm-1.18.0 kubectl-1.18.0 

开启自启
systemctl enable kubelet
```



## 11、部署kubernets master

```
（1）在 master执行

  kubeadm init \
   --apiserver-advertise-address=192.168.137.150 \
   --image-repository registry.aliyuncs.com/google_containers \
   --kubernees-version v1.18.0 \
   --service-cidr=10.96.0.0/12 \
   --pod-network-cidr=10.244.0.0/16

  (2) 在master节点执行如下命令：

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf  $HOME/.kube/config
  sudo chown (id-u):(id-g) $HOME/.kube/config

  kubectl get nodes

```



## 12、加入节点（在nodes上执行）

```
kubeadm join 192.168.137.150:6443 --token abcdef.0123456789abcdef     

--discovery-token-ca-cert-hash sha256:a90d2f26d8dddd737623a281ec4b9ed7fbaba75c7be0e0c08262560634a161e3

```



## 13、安裝flannel网络插件（CNI）

```
   kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```



14、测试kubernetes集群

```
[root@hadoop-001 ~]# kubectl get pods -l app=nginx -o wide

NAME                    READY   STATUS    RESTARTS   AGE    IP            NODE         NOMINATED NODE   READINESS GATES

nginx-f89759699-vs2bk   1/1     Running   0          155m   10.244.2.39   hadoop-003   <none>           <none>

[root@hadoop-001 ~]# kubectl expose deployment nginx --port=80 --type=NodePort

service/nginx exposed

[root@hadoop-001 ~]# kubectl get services nginx

NAME    TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE

nginx   NodePort   10.105.138.161   <none>        80:32761/TCP   23s

[root@hadoop-001 ~]# curl 192.168.137.150:32761
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>


```




如何卸载kubuenetes

使用如下命令《结合使用》

1. kubeadm reset -f
2. modprobe -r ipip
3. lsmod
4. rm -rf ~/.kube/
5. rm -rf /etc/kubernetes/
6. rm -rf /etc/systemd/system/kubelet.service.d
7. rm -rf /etc/systemd/system/kubelet.service
8. rm -rf /usr/bin/kube*
9. rm -rf /etc/cni
10. rm -rf /opt/cni
11. rm -rf /var/lib/etcd
12. rm -rf /var/etcd
13. yum clean all
14. yum remove kube*