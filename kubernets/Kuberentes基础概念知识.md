# 重点：Kubernetes

## Kubernetes的组件

---

---

### master组件

  master组件是集群的控制台

* master组件负责集群中的全局调度
* master组件探测并响应集群事件

#### kube-apiserver

   此 master 组件提供 Kubernetes API。这是Kubernetes控制平台的前端（front-end），可以水平扩展（通过部署更多的实例以达到性能要求）。kubectl / kubernetes dashboard / kuboard 等Kubernetes管理工具就是通过 kubernetes API 实现对 Kubernetes 集群的管理。



#### etcd

一致性高可用的名值键值对存储组件，kubernetes集群所有的配置信息都存储在etcd中。



#### kube-scheduler

监控所有创建尚未分配到节点上的 Pod，并且自动为Pod选择合适的节点去运行。

影响调度的因素有：

* 单个或多个 Pod 的资源需求

* 硬件、软件、策略的限制

* 亲和与反亲和（affinity and anti-affinity）的约定

* 数据本地化要求

* 工作负载间的相互作用

  ​

#### kube-controller-manager

kube-controller-manager中包含的控制器有：

* 节点控制器：负责监听节点停机事件并作出对应相应

* 副本控制器：负责为集群中每一个副本控制器（rc）维护期望的Pod副本数量

* 断点（EndPoints）控制器：负责为断点对象赋值

* Service Account & Token控制器：负责为新的名称空间创建default service  account 和API Access token

  ​

#### cloud-controller-manager

### 

---

---



### node组件

Node 组件运行在每一个节点上（包括 master 节点和 worker 节点），负责维护运行中的 Pod 并提供 Kubernetes 运行时环境。

#### kubelet

运行在每一个集群节点上的代理程序。它确保了Pod中的容器处于运行状态。Kubelet通过多种途径获取PodSpec定义，并确保PodSpec定义中所描述的容器处于运行和健康的状态。

#### kube-proxy

kube-proxy是一个网络代理程序，运行在集群中的每一个节点上。

kube-proxy在节点上维护网络规则，这些网络规则使得您可以在集群内、集群外正确的与Pod进行网络通信。

#### 容器引擎

容器引擎负责运行容器。Kubernetes支持多种容器引擎：Docker、containerd、cri-o、rktlet 以及任何实现了 Kubernetes容器引擎接口 的容器引擎



###                