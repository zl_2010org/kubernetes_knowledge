### DevOps

Development 和 Operations 的组合 

- DevOps 看作开发（软件工程）、技术运营和质量保障（QA）三者的交集。
- 突出重视软件开发人员和运维人员的沟通合作，通过自动化流程来使得软件构建、测试、 发布更加快捷、频繁和可靠。
- DevOps 希望做到的是软件产品交付过程中 IT 工具链的打通，使得各个团队减少时间损 耗，更加高效地协同工作。专家们总结出了下面这个 DevOps 能力图，良好的闭环可以大大 增加整体的产出。 

### CICD是什么 

持续集成     持续部署

### Jenkins 

#### 1.Jenkins安装

```
/var/jenkins_home jenkins的家目录

包含了jenkins的所有配置。

以后要注意备份 /var/jenkins_home （以文件的方式固化的）

```

#### 

```yaml
docker run \
  -u root \
  -d \
  -p 8080:8080 \
  -p 50000:50000 \
  -v jenkins-data:/var/jenkins_home \
  -v /etc/localtime:/etc/localtime:ro \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --restart=always \
  jenkinsci/blueocean
  
#自己构建镜像 RUN的时候就把时区设置好
#如果是别人的镜像，docker hub，UTC； 容器运行时 ， -v
/etc/localtime:/etc/localtime:ro
jenkinsci/jenkins 是没有 blueocean插件的，得自己装
jenkinsci/blueocean：带了的
#/var/run/docker.sock 表示Docker守护程序通过其监听的基于Unix的套接字。 该映射允许
jenkinsci/blueocean 容器与Docker守护进程通信， 如果 jenkinsci/blueocean 容器需要实例化
其他Docker容器，则该守护进程是必需的。 如果运行声明式管道，其语法包含agent部分用 docker；例
如， agent { docker { ... } } 此选项是必需的。
#如果你的jenkins 安装插件装不上。使用这个镜像【 registry.cnqingdao.aliyuncs.com/lfy/jenkins:plugins-blueocean 】默认访问账号/密码是
【admin/admin】
```



安装插件，并配置用户 



#### 2.Jenkins实战 

  代码在本地修改----提交到远程gitee----触发jenkins整个自动化构建流程（打包，测试，发布，部署）

1、准备一个git项目进行测试 

我们以gitee为例，github可能太慢了。需要idea安装gitee插件。或者自己熟悉手动命令也行。
步骤：

- 1、idea创建Spring Boot项目
- 2、VCS - 创建git 仓库
- 3、gitee创建一个空仓库，示例为public
- 4、idea提交内容到gitee
- 5、开发项目基本功能，并在项目中创建一个Jenkinsfile文件
- 6、创建一个名为 devops-java-demo的流水线项目，使用项目自己的流水线 





Jenkins的工作流程
1、先定义一个流水线项目，指定项目的git位置

- 流水线启动

  1、先去git位置自动拉取代码

  2、解析拉取代码里面的Jenkinsfile文件

  3、按照Jenkinsfile指定的流水线开始加工项目 

  ​



```yaml
Jenkins重要的点:

0、jenkins的家目录 /var/jenkins_home 已经被我们docker外部挂载了 ；
/var/lib/docker/volumes/jenkins-data/_data

1、WORKSPACE（工作空间）=/var/jenkins_home/workspace/java-devops-demo
每一个流水线项目，占用一个文件夹位置
BUILD_NUMBER=5；当前第几次构建
WORKSPACE_TMP（临时目录）=/var/jenkins_home/workspace/java-devops-demo@tmp
		1 JOB_URL=http://139.198.9.163:8080/job/java-devops-demo/
		
2、常用的环境如果没有，jenkins配置环境一大堆操作

3、jenkins_url ： http://139.198.27.103:8080/
```



2、远程构建触发 

期望效果： 远程的github代码提交了，jenkins流水线自动触发构建。



实现流程：
1、保证jenkins所在主机能被远程访问
2、jenkins中远程触发需要权限，我们应该使用用户进行授权
3、配置gitee/github，webhook进行触发 



```
#远程构建即使配置了github 的webhook，默认会403.我们应该使用用户进行授权
1、创建一个用户
2、一定随便登陆激活一次
3、生成一个apitoken
http://leifengyang:113620edce6200b9c78ecadb26e9cf122e@139.198.186.134:8080/job/dev
ops-java-demo/build?token=leifengyang

***远程触发： JENKINS_URL /job/simple-java-maven-app/build?token= TOKEN_NAME 请求即可
```



#### 3.流水线语法 

##### 1、基础格式 

```yaml
pipeline {
    agent any
    environment {
    	CC = 'clang'
    } stages {
        stage('Example') {
          steps {
          sh 'printenv'
          sh 'echo $CC'
			}
		}
	}
}
```

##### 2、环境变量 



##### 3、密钥管理 



##### 4、自定义agent 

```xml
//需要安装docker、docker pipeline插件
pipeline {
	agent none
		stages {
			stage('Example Build') {
				agent {
					docker 'maven:3-alpine'
					//args 是指定 docker run 的所有指令
					args '-v /var/jenkins_home/maven/.m2:/root/.m2'
				} steps {
					echo 'Hello, Maven'
					sh 'mvn --version'
				}
		} stage('Example Test') {
                agent { docker 'openjdk:8-jre' }
                steps {
                    echo 'Hello, JDK'
                    sh 'java -version'
			 }
		}
	}
}
```



##### 5、参数化

##### 6、条件表达式

##### 7、修改jenkins插件源 

```
 http://mirror.xmission.com/jenkins/updates/update-center.json
```

jenkins插件中心的插件 




